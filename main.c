/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2022 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <locale.h>
#include <math.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <float.h>
#include <getopt.h>
#include <errno.h>
#include <pthread.h>

#include "edflib.h"
#include "third_party/smarc/smarc.h"


#define PROGRAM_NAME       "edf-resampler"

#define PROGRAM_VERSION    "1.02"

#define MAXPATHLEN         (1024)

#define MAX_RUN_IN_SECS    (10)



volatile sig_atomic_t sig_flag=0;

void signal_catch_func(__attribute__((unused)) int sig)
{
  sig_flag = 1;
}


struct init_pfilter_thrd_arg_struct {
    int chan;
    int sf;
    int srate;
    char ratios_str[4096];
    struct PFilter **pfilt;
} init_pfilter_thrd_args[EDFLIB_MAXSIGNALS];


void * init_pfilter_thread_func(void *args_p)
{
  struct init_pfilter_thrd_arg_struct *args;

  args = (struct init_pfilter_thrd_arg_struct *)args_p;

  if(strlen(args->ratios_str) > 2)
  {
    *args->pfilt = smarc_init_pfilter(args->sf, args->srate, 0.85, 0.01, 100, 0.0000001, args->ratios_str, 0);
  }
  else
  {
    *args->pfilt = smarc_init_pfilter(args->sf, args->srate, 0.85, 0.01, 100, 0.0000001, NULL, 0);
  }

  return NULL;
}


struct run_pfilter_thrd_arg_struct {
    int chan;
    int sf;
    int srate;
    int *idx;
    struct PFilter **pfilt;
    struct PState **pstate;
    double *buf_in;
    double *buf_out;
    int buf_sz_out;
} run_pfilter_thrd_args[EDFLIB_MAXSIGNALS];


void * run_pfilter_thread_func(void *args_p)
{
  struct run_pfilter_thrd_arg_struct *args;

  args = (struct run_pfilter_thrd_arg_struct *)args_p;

  *args->idx += smarc_resample(*args->pfilt, *args->pstate, args->buf_in, args->sf, args->buf_out + *args->idx, args->buf_sz_out);

  return NULL;
}


int copy_edf_params(struct edf_hdr_struct *, int, int, int, int *, int);
int strlcpy(char *, const char *, int);
int strlcat(char *, const char *, int);
void remove_extension_from_filename(char *);


int main(int argc, char **argv)
{
  int i, len, tmp, redo,
      secs,
      run_in_secs,
      err=-1,
      chan=-1,
      chns=0,
      sf[EDFLIB_MAXSIGNALS]={0,},
      idx[EDFLIB_MAXSIGNALS]={0,},
      srate[EDFLIB_MAXSIGNALS]={0,},
      skip[EDFLIB_MAXSIGNALS]={0,},
      seconds_in_file=0,
      verbose=0,
      buf_sz_out[EDFLIB_MAXSIGNALS]={0,},
      hdl_in=-1,
      hdl_out=-1,
      c=0,
      option_index=0,
      n_cpu,
      thrd_num,
      ok,
      datrec_mult=1,
      drec,
      srate_opt_num=0;

  double *buf_in[EDFLIB_MAXSIGNALS]={NULL,},
         *buf_out[EDFLIB_MAXSIGNALS]={NULL,};

  char path_in[MAXPATHLEN]="",
       path_out[MAXPATHLEN]="",
       srate_str[4096]="",
       ratios_str[4096]="",
       *str_ptr=NULL;

  struct edf_hdr_struct hdr;

  struct PFilter *pfilt[EDFLIB_MAXSIGNALS]={NULL,};

  struct PState *pstate[EDFLIB_MAXSIGNALS]={NULL,};

  pthread_t init_pfilter_thrd[EDFLIB_MAXSIGNALS];

  pthread_t run_pfilter_thrd[EDFLIB_MAXSIGNALS];

  n_cpu = sysconf(_SC_NPROCESSORS_ONLN);

  setlocale(LC_ALL, "C");

  signal(SIGINT, signal_catch_func);

  setlinebuf(stdout);
  setlinebuf(stderr);

  if(n_cpu < 1)
  {
    fprintf(stderr, "error: cannot determine the number of cpu-cores on this system\n\n");
    goto OUT_ERROR_1;
  }

  struct option long_options[] = {
    {"inputfile",       required_argument, 0, 0},  /* 0 */
    {"outputfile",      required_argument, 0, 0},  /* 1 */
    {"samplerate",      required_argument, 0, 0},  /* 2 */
    {"ratios",          required_argument, 0, 0},  /* 3 */
    {"verbose",         no_argument,       0, 0},  /* 4 */
    {"help",            no_argument,       0, 0},  /* 5 */
    {0, 0, 0, 0}
  };

  while(1)
  {
    c = getopt_long_only(argc, argv, "", long_options, &option_index);

    if(c == -1)  break;

    if(option_index == 0)  /* inputfile */
    {
      strlcpy(path_in, optarg, MAXPATHLEN);
    }

    if(option_index == 1)  /* outputfile */
    {
      strlcpy(path_out, optarg, MAXPATHLEN);
    }

    if(option_index == 2)  /* samplerate */
    {
      strlcpy(srate_str, optarg, 4096);

      for(srate_opt_num=0; srate_opt_num<EDFLIB_MAXSIGNALS; srate_opt_num++)
      {
        if(!srate_opt_num)
        {
          str_ptr = strtok(srate_str, ",");
        }
        else
        {
          str_ptr = strtok(NULL, ",");
        }

        if(str_ptr == NULL)  break;

        len = strlen(str_ptr);
        for(i=0; i<len; i++)
        {
          if((str_ptr[i] < '0') || (str_ptr[i] > '9'))
          {
            fprintf(stderr, "error: illegal value for samplerate (must be an integer number)\n\n");
            goto OUT_ERROR_1;
          }
        }
        srate[srate_opt_num] = atoi(str_ptr);
        if(srate[srate_opt_num] < 10)
        {
          fprintf(stderr, "error: illegal value for samplerate (must be >= 10 Hz)\n\n");
          goto OUT_ERROR_1;
        }
      }
    }

    if(option_index == 3)  /* ratios */
    {
      strlcpy(ratios_str, optarg, 4096);
    }

    if(option_index == 4)  /* verbose */
    {
      verbose = 1;
    }

    if(option_index == 5)  /* help */
    {
      fprintf(stdout,
              "\n" PROGRAM_NAME " " PROGRAM_VERSION " by Teunis van Beelen\n\n"
              "%s\t<-i inputfilename | --inputfile=inputfilename>\n"
              "\t\t<-s samplefrequency | --samplerate=samplefrequency>\n"
              "\t\t[-r userratios | --ratios=userratios]\n"
              "\t\t[-o outputfilename | --outputfile=outputfilename]\n"
              "\t\t[-v | --verbose]\n"
              "\t\t[-h | --help]\n\n",
              PROGRAM_NAME);

      exit(EXIT_SUCCESS);
    }
  }

  if(srate_opt_num < 1)
  {
    fprintf(stderr, "error: missing argument -s or --samplerate=\n\n");
    goto OUT_ERROR_1;
  }

  if(strlen(path_in) < 5)
  {
    fprintf(stderr, "error: missing argument -i or --intputfile\n\n");
    goto OUT_ERROR_1;
  }

  if(!strlen(path_out))
  {
    strlcpy(path_out, path_in, MAXPATHLEN);

    remove_extension_from_filename(path_out);

    strlcat(path_out, "_out.edf", MAXPATHLEN);
  }

  if(strlen(path_out) < 5)
  {
    fprintf(stderr, "error: missing argument outputfile\n\n");
    goto OUT_ERROR_1;
  }

  if((strlen(path_in) == strlen(path_out)) && (!strncmp(path_in, path_out, strlen(path_in) - 4)))
  {
    fprintf(stderr, "error: inputfilename equals outputfilename\n\n");
    goto OUT_ERROR_1;
  }

  if(srate[0] < 10)
  {
    fprintf(stderr, "error: samplerate must be >= 10 Hz\n\n");
    goto OUT_ERROR_1;
  }

  if(srate[0] > 1000000)
  {
    fprintf(stderr, "error: samplerate given is absurdly high\n\n");
    goto OUT_ERROR_1;
  }

  if(srate_opt_num > 1)
  {
    if(strlen(ratios_str))
    {
      fprintf(stderr, "error: cannot use ratios when using different output samplerates\n\n");
      goto OUT_ERROR_1;
    }
  }

  if(edfopen_file_readonly(path_in, &hdr, EDFLIB_READ_ALL_ANNOTATIONS))
  {
    fprintf(stderr, "cannot open inputfile,\n");

    switch(hdr.filetype)
    {
      case EDFLIB_MALLOC_ERROR                : fprintf(stderr, "EDFlib: malloc error\n\n");
                                                break;
      case EDFLIB_NO_SUCH_FILE_OR_DIRECTORY   : fprintf(stderr, "EDFlib: can not open file, no such file or directory\n\n");
                                                break;
      case EDFLIB_FILE_CONTAINS_FORMAT_ERRORS : fprintf(stderr, "EDFlib: the file is not EDF(+) or BDF(+) compliant\n"
                                                                "(it contains format errors)\n\n");
                                                break;
      case EDFLIB_MAXFILES_REACHED            : fprintf(stderr, "EDFlib: too many files opened\n\n");
                                                break;
      case EDFLIB_FILE_READ_ERROR             : fprintf(stderr, "EDFlib: a read error occurred\n\n");
                                                break;
      case EDFLIB_FILE_ALREADY_OPENED         : fprintf(stderr, "EDFlib: file has already been opened\n\n");
                                                break;
      default                                 : fprintf(stderr, "EDFlib: unknown error\n\n");
                                                break;
    }

    goto OUT_ERROR_1;
  }

  if((hdr.filetype == EDFLIB_FILETYPE_BDF) || (hdr.filetype == EDFLIB_FILETYPE_BDFPLUS))
  {
    path_out[strlen(path_out)-3] = 'b';
  }

  hdl_in = hdr.handle;

  chns = hdr.edfsignals;
  if(chns < 1)
  {
    fprintf(stderr, "error: inputfile does not contain a signal\n\n");
    goto OUT_ERROR_1;
  }

  if(srate_opt_num == 1)
  {
    for(chan=1; chan<chns; chan++)
    {
      srate[chan] = srate[0];
    }
  }
  else if(srate_opt_num != chns)
    {
      fprintf(stderr, "error: number of samplerate values given does not match number of signals in inputfile:\n"
                      "number of samplerate values: %i\n"
                      "number of signals in inputfile: %i\n\n", srate_opt_num, chns);
      goto OUT_ERROR_1;
    }

  seconds_in_file = hdr.file_duration / EDFLIB_TIME_DIMENSION;
  if(seconds_in_file < 1)
  {
    fprintf(stderr, "error: inputfile duration is shorter than 1 sec.: %i\n\n", seconds_in_file);
    goto OUT_ERROR_1;
  }

  for(chan=0; chan<chns; chan++)
  {
    sf[chan] = (hdr.signalparam[chan].smp_in_datarecord * EDFLIB_TIME_DIMENSION) / hdr.datarecord_duration;
    if(sf[chan] < 1)
    {
      fprintf(stderr, "error: input samplerate of signal %i is less than 1 Hz: %i\n\n", chan + 1, sf[chan]);
      goto OUT_ERROR_1;
    }

    if((hdr.signalparam[chan].smp_in_datarecord * EDFLIB_TIME_DIMENSION) % hdr.datarecord_duration)
    {
      fprintf(stderr, "error: input samplerate of signal %i is not an integer\n\n", chan + 1);
      goto OUT_ERROR_1;
    }
  }

  datrec_mult=10;

  redo = 0;

INIT_1:

  for(chan=0; chan<chns; chan++)
  {
    if(srate[chan] % datrec_mult)
    {
      if(datrec_mult == 100)
      {
        datrec_mult = 80;
      }
      else if(datrec_mult == 80)
        {
          datrec_mult = 50;
        }
        else if(datrec_mult == 50)
          {
            datrec_mult = 40;
          }
          else if(datrec_mult == 40)
            {
              datrec_mult = 20;
            }
            else if(datrec_mult == 20)
              {
                datrec_mult = 10;
              }
              else if(datrec_mult == 10)
              {
                datrec_mult = 8;
              }
              else if(datrec_mult == 8)
                {
                  datrec_mult = 5;
                }
                else if(datrec_mult == 5)
                  {
                    datrec_mult = 4;
                  }
                  else if(datrec_mult == 4)
                    {
                      datrec_mult = 2;
                    }
                    else if(datrec_mult == 2)
                      {
                        datrec_mult = 1;
                        break;
                      }

       chan = -1;
    }
  }

  for(chan=0, tmp=0; chan<chns; chan++)
  {
    tmp += srate[chan];
  }

  tmp /= datrec_mult;

  if(tmp > (5 * 1024 * 1024))
  {
    if(!redo)
    {
      datrec_mult = 100;

      redo = 1;

      goto INIT_1;
    }

    fprintf(stderr, "error: the new samplerate(s) will cause the datarecord size to exceed the limit\n\n");
    goto OUT_ERROR_1;
  }

/*************************************************************************************/

  if(verbose)
  {
    fprintf(stdout, "setting up filters...\n");
  }

  for(thrd_num=0, chan=0, err=0; chan<chns; chan++)
  {
    if(sf[chan] == srate[chan])
    {
      skip[chan] = 1;

      continue;
    }

    skip[chan] = 0;

    if(thrd_num == n_cpu)
    {
      for(thrd_num=0; thrd_num<n_cpu; thrd_num++)  /* wait untill the threads have finished */
      {
        pthread_join(init_pfilter_thrd[thrd_num], NULL);
      }

      thrd_num = 0;
    }

    init_pfilter_thrd_args[chan].chan = chan;
    init_pfilter_thrd_args[chan].sf = sf[chan];
    init_pfilter_thrd_args[chan].srate = srate[chan];
    init_pfilter_thrd_args[chan].pfilt = &pfilt[chan];
    strlcpy(init_pfilter_thrd_args[chan].ratios_str, ratios_str, 4096);

    err = pthread_create(&init_pfilter_thrd[thrd_num], NULL, init_pfilter_thread_func, (void *)&init_pfilter_thrd_args[chan]);
    if(err)
    {
      fprintf(stderr,"error: pthread_create() returns: %i   file: %s line: %i\n", err, __FILE__, __LINE__);
    }

    thrd_num++;
  }

  for(i=0; i<thrd_num; i++)  /* wait untill the remaining threads have finished */
  {
    pthread_join(init_pfilter_thrd[i], NULL);
  }

  if(err)
  {
    goto OUT_ERROR_1;
  }

  for(chan=0; chan<chns; chan++)
  {
    if(skip[chan])
    {
      pfilt[chan] = NULL;

      continue;
    }

    if(pfilt[chan] == NULL)
    {
      fprintf(stderr,"error: pfilter == NULL  signal: %i   file: %s line: %i\n", chan + 1, __FILE__, __LINE__);
      goto OUT_ERROR_1;
    }
  }

  for(chan=0; chan<chns; chan++)
  {
    if(skip[chan])
    {
      buf_sz_out[chan] = srate[chan];

      pstate[chan] = NULL;

      if(verbose)
      {
        fprintf(stdout, "signal %i: no conversion, samplerate is the same\n", chan + 1);
      }

      continue;
    }

    pstate[chan] = smarc_init_pstate(pfilt[chan]);
    if(pstate[chan] == NULL)
    {
      fprintf(stderr, "error: smarc_init_pstate()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      goto OUT_ERROR_1;
    }

    if(smarc_get_fs_in(pfilt[chan]) != sf[chan])
    {
      fprintf(stderr, "error: smarc_init_pfilter()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      goto OUT_ERROR_1;
    }

    if(verbose)
    {
      fprintf(stdout, "signal %i:\n", chan + 1);

      smarc_print_pfilter(pfilt[chan]);
    }

    if(smarc_get_fs_out(pfilt[chan]) != srate[chan])
    {
      fprintf(stderr, "error: smarc_init_pfilter()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      goto OUT_ERROR_1;
    }

    buf_sz_out[chan] = smarc_get_output_buffer_size(pfilt[chan], sf[chan]);
    if(buf_sz_out[chan] < srate[chan])
    {
      fprintf(stderr, "error: smarc_init_pfilter()   signal %i   sf: %i   buf_sz_out: %i   file: %s line: %i\n\n",
              chan + 1, sf[chan], buf_sz_out[chan], __FILE__, __LINE__);
      goto OUT_ERROR_1;
    }
  }

  if((hdr.filetype == EDFLIB_FILETYPE_EDF) || (hdr.filetype == EDFLIB_FILETYPE_EDFPLUS))
  {
    hdl_out = edfopen_file_writeonly(path_out, EDFLIB_FILETYPE_EDFPLUS, chns);
  }
  else
  {
    hdl_out = edfopen_file_writeonly(path_out, EDFLIB_FILETYPE_BDFPLUS, chns);
  }
  if(hdl_out < 0)
  {
    fprintf(stderr, "error: can not create outputfile, errno: %i   path: %s\n\n", hdl_out, path_out);
    goto OUT_ERROR_1;
  }

  if(copy_edf_params(&hdr, hdl_in, hdl_out, chns, srate, datrec_mult))
  {
    goto OUT_ERROR_1;
  }

  for(chan=0; chan<chns; chan++)
  {
    buf_in[chan] = (double *)calloc(1, sf[chan] * sizeof(double));
    if(buf_in[chan] == NULL)
    {
      fprintf(stderr, "error: calloc()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      goto OUT_ERROR_1;
    }

    buf_out[chan] = (double *)calloc(1, (buf_sz_out[chan] + (srate[chan] * MAX_RUN_IN_SECS)) * sizeof(double));
    if(buf_out[chan] == NULL)
    {
      fprintf(stderr, "error: calloc()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      goto OUT_ERROR_1;
    }

    idx[chan] = 0;
  }

/*************************************************************************************/

  if(verbose)
  {
    fprintf(stdout, "starting conversion...\n");
  }

  for(secs=0, run_in_secs=0; secs<seconds_in_file; secs++)
  {
    for(chan=0; chan<chns; chan++)
    {
      if(skip[chan])
      {
        if(edfread_digital_samples(hdl_in, chan, sf[chan], (int *)(buf_in[chan])) != sf[chan])
        {
          fprintf(stderr, "error: edfread_digital_samples() at second: %i   signal: %i   sf: %i   file: %s line: %i\n\n",
                  secs, chan + 1, sf[chan], __FILE__, __LINE__);
          goto OUT_ERROR_1;
        }
      }
      else
      {
        if(edfread_physical_samples(hdl_in, chan, sf[chan], buf_in[chan]) != sf[chan])
        {
          fprintf(stderr, "error: edfread_physical_samples() at second: %i   signal: %i   sf: %i   file: %s line: %i\n\n",
                  secs, chan + 1, sf[chan], __FILE__, __LINE__);
          goto OUT_ERROR_1;
        }
      }
    }

    for(thrd_num=0, chan=0, err=0; chan<chns; chan++)
    {
      if(skip[chan])
      {
        memcpy(&((int *)(buf_out[chan]))[idx[chan]], buf_in[chan], srate[chan] * sizeof(int));

        idx[chan] += srate[chan];

        continue;
      }

      if(thrd_num == n_cpu)
      {
        for(thrd_num=0; thrd_num<n_cpu; thrd_num++)  /* wait untill the threads have finished */
        {
          pthread_join(run_pfilter_thrd[thrd_num], NULL);
        }

        thrd_num = 0;
      }

      run_pfilter_thrd_args[chan].chan = chan;
      run_pfilter_thrd_args[chan].sf = sf[chan];
      run_pfilter_thrd_args[chan].srate = srate[chan];
      run_pfilter_thrd_args[chan].idx = &idx[chan];
      run_pfilter_thrd_args[chan].pfilt = &pfilt[chan];
      run_pfilter_thrd_args[chan].pstate = &pstate[chan];
      run_pfilter_thrd_args[chan].buf_in = buf_in[chan];
      run_pfilter_thrd_args[chan].buf_out = buf_out[chan];
      run_pfilter_thrd_args[chan].buf_sz_out = buf_sz_out[chan];

      err = pthread_create(&run_pfilter_thrd[thrd_num], NULL, run_pfilter_thread_func, (void *)&run_pfilter_thrd_args[chan]);
      if(err)
      {
        fprintf(stderr,"error: pthread_create() returns: %i   file: %s line: %i\n", err, __FILE__, __LINE__);
      }

      thrd_num++;
    }

    for(i=0; i<thrd_num; i++)  /* wait untill the remaining threads have finished */
    {
      pthread_join(run_pfilter_thrd[i], NULL);
    }

    if(err)
    {
      goto OUT_ERROR_1;
    }

    if(sig_flag)
    {
      fprintf(stderr, "interrupted by user\n\n");
      goto OUT_ERROR_1;
    }

    for(chan=0, ok=1; chan<chns; chan++)
    {
      if(idx[chan] < srate[chan])
      {
        ok = 0;

        break;
      }
    }

    if(!ok)
    {
      if(++run_in_secs < MAX_RUN_IN_SECS)
      {
        continue;
      }
      else
      {
        fprintf(stderr, "internal error: not enough run-in seconds   file: %s line: %i\n\n", __FILE__, __LINE__);
        goto OUT_ERROR_1;
      }
    }

    for(drec=0; drec<datrec_mult; drec++)
    {
      for(chan=0; chan<chns; chan++)
      {
        if(skip[chan])
        {
          if(edfwrite_digital_samples(hdl_out, (int *)(buf_out[chan])))
          {
            fprintf(stderr, "error: edfwrite_digital_samples() at second: %i   file: %s line: %i\n\n", secs, __FILE__, __LINE__);
            goto OUT_ERROR_1;
          }
        }
        else
        {
          if(edfwrite_physical_samples(hdl_out, buf_out[chan]))
          {
            fprintf(stderr, "error: edfwrite_physical_samples() at second: %i   file: %s line: %i\n\n", secs, __FILE__, __LINE__);
            goto OUT_ERROR_1;
          }
        }

        idx[chan] -= (srate[chan] / datrec_mult);
        if(idx[chan] < 1)
        {
          fprintf(stderr, "internal error: idx[%i]: %i at sec: %i  file: %s line: %i\n\n", chan, idx[chan], secs, __FILE__, __LINE__);
          goto OUT_ERROR_1;
        }

        if(skip[chan])
        {
          memmove(buf_out[chan], ((int *)(buf_out[chan])) + (srate[chan] / datrec_mult), idx[chan] * sizeof(int));
        }
        else
        {
          memmove(buf_out[chan], buf_out[chan] + (srate[chan] / datrec_mult), idx[chan] * sizeof(double));
        }
      }
    }
  }

  for(secs=0; secs<run_in_secs; secs++)
  {
    for(chan=0; chan<chns; chan++)
    {
      if(skip[chan])
      {
        idx[chan] += srate[chan];
      }
      else
      {
        memset(buf_in[chan], 0, sf[chan] * sizeof(double));

        idx[chan] += smarc_resample(pfilt[chan], pstate[chan], buf_in[chan], sf[chan], buf_out[chan] + idx[chan], buf_sz_out[chan]);
      }
    }

    for(drec=0; drec<datrec_mult; drec++)
    {
      for(chan=0; chan<chns; chan++)
      {
        if(skip[chan])
        {
          if(edfwrite_digital_samples(hdl_out, (int *)(buf_out[chan])))
          {
            fprintf(stderr, "error: edfwrite_digital_samples() at second: %i   file: %s line: %i\n\n", i, __FILE__, __LINE__);
            goto OUT_ERROR_1;
          }
        }
        else
        {
          if(edfwrite_physical_samples(hdl_out, buf_out[chan]))
          {
            fprintf(stderr, "error: edfwrite_physical_samples() at second: %i   file: %s line: %i\n\n", i, __FILE__, __LINE__);
            goto OUT_ERROR_1;
          }
        }

        idx[chan] -= (srate[chan] / datrec_mult);

        if(skip[chan])
        {
          memmove(buf_out[chan], ((int *)(buf_out[chan])) + (srate[chan] / datrec_mult), idx[chan] * sizeof(int));
        }
        else
        {
          memmove(buf_out[chan], buf_out[chan] + (srate[chan] / datrec_mult), idx[chan] * sizeof(double));
        }
      }
    }
  }

/*************************************************************************************/

  edfclose_file(hdl_in);

  edfclose_file(hdl_out);

  for(chan=0; chan<chns; chan++)
  {
    free(buf_in[chan]);

    free(buf_out[chan]);

    if(pfilt[chan] != NULL)
    {
      smarc_destroy_pfilter(pfilt[chan]);
    }

    if(pstate[chan] != NULL)
    {
      smarc_destroy_pstate(pstate[chan]);
    }
  }

  return EXIT_SUCCESS;

OUT_ERROR_1:

  edfclose_file(hdl_in);

  edfclose_file(hdl_out);

  for(chan=0; chan<chns; chan++)
  {
    free(buf_in[chan]);

    free(buf_out[chan]);

    if(pfilt[chan] != NULL)
    {
      smarc_destroy_pfilter(pfilt[chan]);
    }

    if(pstate[chan] != NULL)
    {
      smarc_destroy_pstate(pstate[chan]);
    }
  }

  return EXIT_FAILURE;
}


int copy_edf_params(struct edf_hdr_struct *hdr, int hdl_in, int hdl_out, int chns, int *sr, int drec_mult)
{
  int i, n, chan, datrecs_in_file;

  struct edf_annotation_struct annot;

  for(chan=0; chan<chns; chan++)
  {
    if(edf_set_samplefrequency(hdl_out, chan, sr[chan] / drec_mult))
    {
      fprintf(stderr, "error: edf_set_samplefrequency()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_physical_maximum(hdl_out, chan, hdr->signalparam[chan].phys_max))
    {
      fprintf(stderr, "error: edf_set_physical_maximum()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_physical_minimum(hdl_out, chan, hdr->signalparam[chan].phys_min))
    {
      fprintf(stderr, "error: edf_set_physical_minimum()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_digital_maximum(hdl_out, chan, hdr->signalparam[chan].dig_max))
    {
      fprintf(stderr, "error: edf_set_digital_maximum()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_digital_minimum(hdl_out, chan, hdr->signalparam[chan].dig_min))
    {
      fprintf(stderr, "error: edf_set_digital_minimum()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_label(hdl_out, chan, hdr->signalparam[chan].label))
    {
      fprintf(stderr, "error: edf_set_label()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_physical_dimension(hdl_out, chan, hdr->signalparam[chan].physdimension))
    {
      fprintf(stderr, "error: edf_set_physical_dimension()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_prefilter(hdl_out, chan, hdr->signalparam[chan].prefilter))
    {
      fprintf(stderr, "error: edf_set_prefilter()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_transducer(hdl_out, chan, hdr->signalparam[chan].transducer))
    {
      fprintf(stderr, "error: edf_set_transducer()   signal %i   file: %s line: %i\n\n", chan + 1, __FILE__, __LINE__);
      return -1;
    }
  }

  if(edf_set_datarecord_duration(hdl_out,  100000LL / drec_mult))
  {
    fprintf(stderr, "error: edf_set_datarecord_duration() file: %s line: %i\n\n", __FILE__, __LINE__);
    return -1;
  }

  if(edf_set_startdatetime(hdl_out, hdr->startdate_year, hdr->startdate_month, hdr->startdate_day, hdr->starttime_hour, hdr->starttime_minute, hdr->starttime_second))
  {
    fprintf(stderr, "error: edf_set_startdatetime() file: %s line: %i\n\n", __FILE__, __LINE__);
    return -1;
  }

  if(edf_set_subsecond_starttime(hdl_out, hdr->starttime_subsecond))
  {
    fprintf(stderr, "error: edf_set_subsecond_starttime() file: %s line: %i\n\n", __FILE__, __LINE__);
    return -1;
  }

  if((hdr->filetype == EDFLIB_FILETYPE_EDFPLUS) || (hdr->filetype == EDFLIB_FILETYPE_BDFPLUS))
  {
    if(edf_set_patientname(hdl_out, hdr->patient_name))
    {
      fprintf(stderr, "error: edf_set_patientname()   file: %s line: %i\n\n", __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_patientcode(hdl_out, hdr->patientcode))
    {
      fprintf(stderr, "error: edf_set_patientcode()   file: %s line: %i\n\n", __FILE__, __LINE__);
      return -1;
    }

    if(!strcmp(hdr->gender, "Male"))
    {
      if(edf_set_gender(hdl_out, 1))
      {
        fprintf(stderr, "error: edf_set_gender()   file: %s line: %i\n\n", __FILE__, __LINE__);
        return -1;
      }
    }
    else if(!strcmp(hdr->gender, "Female"))
      {
        if(edf_set_gender(hdl_out, 0))
        {
          fprintf(stderr, "error: edf_set_gender()   file: %s line: %i\n\n", __FILE__, __LINE__);
          return -1;
        }
      }

    if(hdr->birthdate_day)
    {
      if(edf_set_birthdate(hdl_out, hdr->birthdate_year, hdr->birthdate_month, hdr->birthdate_day))
      {
        fprintf(stderr, "error: edf_set_birthdate()   file: %s line: %i\n\n", __FILE__, __LINE__);
        return -1;
      }
    }

    if(edf_set_patient_additional(hdl_out, hdr->patient_additional))
    {
      fprintf(stderr, "error: edf_set_patient_additional()   file: %s line: %i\n\n", __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_admincode(hdl_out, hdr->admincode))
    {
      fprintf(stderr, "error: edf_set_admincode()   file: %s line: %i\n\n", __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_technician(hdl_out, hdr->technician))
    {
      fprintf(stderr, "error: edf_set_technician()   file: %s line: %i\n\n", __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_equipment(hdl_out, hdr->equipment))
    {
      fprintf(stderr, "error: edf_set_equipment()   file: %s line: %i\n\n", __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_recording_additional(hdl_out, hdr->recording_additional))
    {
      fprintf(stderr, "error: edf_set_recording_additional()   file: %s line: %i\n\n", __FILE__, __LINE__);
      return -1;
    }
  }
  else  /* legacy EDF or BDF */
  {
    if(edf_set_patient_additional(hdl_out, hdr->patient))
    {
      fprintf(stderr, "error: edf_set_patient_additional()   file: %s line: %i\n\n", __FILE__, __LINE__);
      return -1;
    }

    if(edf_set_recording_additional(hdl_out, hdr->recording))
    {
      fprintf(stderr, "error: edf_set_recording_additional()   file: %s line: %i\n\n", __FILE__, __LINE__);
      return -1;
    }
  }

  datrecs_in_file = (hdr->file_duration / EDFLIB_TIME_DIMENSION) * drec_mult;

  n = hdr->annotations_in_file / datrecs_in_file;
  if(hdr->annotations_in_file % datrecs_in_file)
  {
    n++;
  }
  if(n < 1)
  {
    n = 1;
  }
  else if(n > 64)
    {
      n = 64;
    }

  if(edf_set_number_of_annotation_signals(hdl_out, n))
  {
    fprintf(stderr, "error: edf_set_number_of_annotation_signals() file: %s line: %i\n\n", __FILE__, __LINE__);
    return -1;
  }

  for(i=0; i<hdr->annotations_in_file; i++)
  {
    if(edf_get_annotation(hdl_in, i, &annot))
    {
      fprintf(stderr, "error: edf_get_annotation() file: %s line: %i\n\n", __FILE__, __LINE__);
      return -1;
    }

    if(edfwrite_annotation_utf8(hdl_out, annot.onset / 1000LL, annot.duration_l / 1000LL, annot.annotation))
    {
      fprintf(stderr, "error: edfwrite_annotation_utf8() file: %s line: %i\n\n", __FILE__, __LINE__);
      return -1;
    }
  }

  return 0;
}


int strlcpy(char *dst, const char *src, int sz)
{
  int srclen;

  sz--;

  srclen = strlen(src);

  if(srclen > sz)  srclen = sz;

  memcpy(dst, src, srclen);

  dst[srclen] = 0;

  return srclen;
}


int strlcat(char *dst, const char *src, int sz)
{
  int srclen,
      dstlen;

  dstlen = strlen(dst);

  sz -= dstlen + 1;

  if(sz < 1)  return dstlen;

  srclen = strlen(src);

  if(srclen > sz)  srclen = sz;

  memcpy(dst + dstlen, src, srclen);

  dst[dstlen + srclen] = 0;

  return (dstlen + srclen);
}


void remove_extension_from_filename(char *str)
{
  int i, len;

  len = strlen(str);

  if(len < 1)
  {
    return;
  }

  for(i=len-1; i>=0; i--)
  {
    if((str[i]=='/') || (str[i]=='\\'))
    {
      return;
    }

    if(str[i]=='.')
    {
      str[i] = 0;

      return;
    }
  }
}













